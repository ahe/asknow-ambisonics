using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectee : MonoBehaviour
{
    private Outline _outline;
    public bool isActive = true;

    private void Awake()
    {
        _outline = GetComponent<Outline>();
    }

    public void ChangeStatus()
    {
        ChangeStatus(!isActive);
    }

    public void ChangeStatus(bool status)
    {
        isActive = status;
        _outline.OutlineColor = isActive ? new Color(0, 1, 0) : new Color(1, 0, 0);
    }

}
