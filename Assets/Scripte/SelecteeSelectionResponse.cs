﻿using UnityEngine;

 class SelecteeSelectionResponse : MonoBehaviour, ISelectionResponse
{
    public void OnSelect(Transform selection)
    {
        var selectee = selection.GetComponent<Selectee>();
        selectee.ChangeStatus();
    }

    public void OnDeselect(Transform selection)
    {
         var selectee = selection.GetComponent<Selectee>();
         selectee.ChangeStatus();
    }
}