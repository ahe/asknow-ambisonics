﻿using System;
using UnityEngine;


public class SelectionManager : MonoBehaviour
{

    private IRayProvider _rayProvider;
    private ISelector _selector;
    
    private Transform _currentSelection;

    private ISelectionResponse _selectionResponse;

    private void Awake()
    {
        _selectionResponse = GetComponent<ISelectionResponse>();
        _rayProvider = GetComponent<IRayProvider>();
        _selector = GetComponent<ISelector>();
    }

    private void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            _selector.Check(_rayProvider.CreateRay());
            _currentSelection = _selector.GetSelection();

            if (_currentSelection != null) _selectionResponse.OnSelect(_currentSelection);
        }

    }

  

   
}