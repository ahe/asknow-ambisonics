using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowFront : MonoBehaviour
{
    [SerializeField] private Transform loudspeaker;
    [SerializeField] private float azimuth;
    [SerializeField] private float elevation;
    
    
    private void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = new Color(1, 1, 0, 0.75F);
        var transform1 = transform;
        Gizmos.DrawRay(transform1.position, ( transform1.forward));
        if (loudspeaker==null)
            return;
        GetAnglesForDirectivity(loudspeaker.position, transform.position, transform.rotation, 
             out azimuth, out elevation);

    }
    
    private void GetAnglesForDirectivity(Vector3 sourcePos, Vector3 receiverPos, Quaternion receiverRot,
        out float azimuth, out float elevation)
    {
        // var viewAngle = Vector3.back;
        var viewAngle = Vector3.back;
        var refView = receiverRot * viewAngle;
        var refUp = receiverRot * Vector3.up;
        // var diffVector = ssWalls[i].transform.position - transform.parent.position;
        var diffVector = sourcePos - receiverPos;
        Vector3 wLocal;
        wLocal.x = Vector3.Dot(diffVector, Vector3.Cross(refView, refUp));
        wLocal.y = Vector3.Dot(diffVector, refUp);
        wLocal.z = Vector3.Dot(diffVector, refView);

        azimuth = Mathf.Atan2(-wLocal.x, -wLocal.z) * 180.0f / Mathf.PI;
        elevation = Mathf.Asin(Vector3.Normalize(wLocal).y) * 180.0f / Mathf.PI;
    }

}
