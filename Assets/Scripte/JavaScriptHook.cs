using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class JavaScriptHook : MonoBehaviour
{
    public GameObject parent;
    public Selectee[] allSelectees;
    public bool[] status;
    public string statusString;

    [DllImport("__Internal")]
    private static extern void SendLoudspeakerData(string str);

    
    private void Awake()
    {
        allSelectees = parent.GetComponentsInChildren<Selectee>();
        status = new bool[allSelectees.Length];
    }
    
    

    public void SelectAll()
    {
        for (int i = 0; i < allSelectees.Length; i++)
        {
            allSelectees[i].ChangeStatus(true);
        }
    }
    public void DeselectAll()
    {
        for (int i = 0; i < allSelectees.Length; i++)
        {
            allSelectees[i].ChangeStatus(false);
        }
        
    }

    public void Update()
    {
        UpdateStatus();
    }

    private void UpdateStatus()
    {
        var hasChanged = false;
        statusString = "";
        for (var i = 0; i < allSelectees.Length; i++)
        {
            if(status[i] != allSelectees[i].isActive)
            {
                status[i] = allSelectees[i].isActive;
                hasChanged = true;
            }

            statusString += status[i] ? "0" : "1";
        }

        if (hasChanged)
        {
            SendLoudspeakerData(statusString);

        }
    }
}
