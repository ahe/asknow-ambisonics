﻿using UnityEngine;

public class MouseScreenRayProvider : MonoBehaviour, IRayProvider
{
    public Ray CreateRay()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return ray;
    }
}